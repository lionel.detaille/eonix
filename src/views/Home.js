import React from 'react';
import ButtonIncrement from './increment';
import ButtonDecrement from './decrement';


function Home({ counter, onIncrement, onDecrement }) {
  return (
    <div>
      <ButtonIncrement counter={counter} action={onIncrement} />
      <br />
      <ButtonDecrement counter={counter} action={onDecrement} />
    </div>
  );
}
export default Home;




