import React from 'react';

function ButtonDecrement(props) {
    return (
        <div>
            <button onClick={props.counter >= 1 ? props.action : null}>
                Décrémenter
            </button>
        </div>
    );
};

export default ButtonDecrement;