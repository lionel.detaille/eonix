import React from 'react';

function ButtonIncrement(props) {
    return (
        <div>
            <button onClick={props.action}>
                Vous avez cliqué {props.counter} fois
            </button>
        </div>
    );
};

export default ButtonIncrement;